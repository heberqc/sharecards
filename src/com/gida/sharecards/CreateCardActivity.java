package com.gida.sharecards;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.gida.sharecards.bean.DatosUsuario;
import com.gida.sharecards.others.ChangePhotoHelper;
import com.gida.sharecards.vcard.VCard;

import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import android.widget.ImageView;

@SuppressLint("DefaultLocale")
public class CreateCardActivity extends Activity implements OnClickListener {

	private DatosUsuario new_card;
	private EditText txt_cardName, txt_name, txt_surname, txt_home_cell,
			txt_notes;
	private ImageView photoIcon;
	private long _id = 0;
	Boolean forEdit = true;
	String str_state;
	String namePhoto;

	/** procedimiento de creaci�n del Activity */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_card);
		new_card = null;
		namePhoto = VCard.getDir(getApplication()).getAbsolutePath()
				+ "/photo.jpg";

		/** vinculando los campos de entrada */
		txt_cardName = (EditText) findViewById(R.id.txt_card_name);
		txt_name = (EditText) findViewById(R.id.txt_name);
		txt_surname = (EditText) findViewById(R.id.txt_surname);
		txt_home_cell = (EditText) findViewById(R.id.txt_cell_number);
		txt_notes = (EditText) findViewById(R.id.txt_notes);
		photoIcon = (ImageView) findViewById(R.id.photo);
		registerForContextMenu(photoIcon);
		/** verificaciOn si el activity fue para editar o crear */
		Bundle extras = getIntent().getExtras();
		if (extras == null) {
			forEdit = false;
		}

		/**
		 * si no tiene extras, significa que es nuevo registro, y si sI tiene es
		 * porque se va editar
		 */
		if (forEdit) {
			str_state = getResources().getString(R.string.updated);
			setTitle(getResources().getString(R.string.update_card));

			DatosUsuario temp = (DatosUsuario) getIntent()
					.getSerializableExtra(DatosUsuario.CLASS_NAME);
			setDatosUsuarioToActivity(temp);
			txt_cardName.setEnabled(false);
			/** esconder el teclado */
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		} else {
			str_state = getResources().getString(R.string.created);
			setTitle(getResources().getString(
					R.string.title_activity_create_card));
			txt_cardName.requestFocus();
		}

	}

	private void guardar() {
		// borrando mensajes de campos vacIos
		txt_cardName.setError(null);
		txt_surname.setError(null);
		// comprobaciOn de los campos, 1ero nombre de la tarjeta
		if (txt_cardName.getText().length() == 0) {
			txt_cardName
					.setError(getResources().getString(R.string.empty_card));
			txt_cardName.requestFocus();
			return;
		} else {
			// segunda comprobaciOn, nombre o apellido de la persona
			if (txt_name.getText().length() == 0
					&& txt_surname.getText().length() == 0) {
				txt_name.setError(getResources()
						.getString(R.string.empty_names));
				txt_name.requestFocus();
				return;
			} else {
				// si todo estA bien, se genera una nueva tarjeta
				createCard();
				finish();
			}
		}
	}

	private void cancelCreate() {
		Intent data = new Intent();
		data.putExtra(DatosUsuario.CLASS_NAME, new_card);
		setResult(MainActivity.OTHER, data);
		Log.i("createCard", "Cancelar");
		finish();
	}

	/** cargar los menUs */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (forEdit) {
			getMenuInflater().inflate(R.menu.menu_editar, menu);
		} else {
			getMenuInflater().inflate(R.menu.menu_crear, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		/**
		 * opciOn de eliminaciOn, solo aparece cuando se iniciO la actividad
		 * para editar, no aparece en el caso de nueva tarjeta
		 */
		case R.id.action_delete:
			Intent data = new Intent();
			data.putExtra(DatosUsuario.CLASS_NAME, getDatosUsuario());
			setResult(MainActivity.DELETE_CARD, data);
			Log.i("ACTION_DELETE", "Se enviaron los datos");

			// Toast
			Toast.makeText(
					getApplicationContext(),
					getResources().getString(R.string.action_delete) + " "
							+ txt_cardName.getText(), Toast.LENGTH_SHORT)
					.show();
			finish();
			break;

		case R.id.action_guardar:
			guardar();
			break;

		case R.id.action_cancelar:
			cancelCreate();
			break;

		default:
			return super.onOptionsItemSelected(item);
		}

		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.menu_change_photo, menu);
		menu.setHeaderTitle(R.string.changePhoto);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		int code = ChangePhotoHelper.TAKE_PICTURE;
		switch (item.getItemId()) {
		case R.id.takePhoto:
			Uri output = Uri.fromFile(new File(namePhoto));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
			break;
		case R.id.selectPhoto:
			intent = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
			code = ChangePhotoHelper.SELECT_PICTURE;
			break;
		case R.id.takePreview:
			break;
		}
		startActivityForResult(intent, code);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == ChangePhotoHelper.TAKE_PICTURE) {
			/**
			 * Si se reciben datos en el intent tenemos una vista previa
			 * (thumbnail)
			 */
			if (data != null) {
				/**
				 * En el caso de una vista previa, obtenemos el extra �data� del
				 * intent y lo mostramos en el ImageView
				 */
				if (data.hasExtra("data")) {
					ImageView iv = (ImageView) findViewById(R.id.photo);
					iv.setImageBitmap((Bitmap) data.getParcelableExtra("data"));
				}
				/**
				 * De lo contrario es una imagen completa
				 */
			} else {
				/**
				 * A partir del nombre del archivo ya definido lo buscamos y
				 * creamos el bitmap para el ImageView
				 */
				ImageView iv = (ImageView) findViewById(R.id.photo);
				iv.setImageBitmap(BitmapFactory.decodeFile(namePhoto));
				/**
				 * Para guardar la imagen en la galer�a, utilizamos una conexi�n
				 * a un MediaScanner
				 */
				new MediaScannerConnectionClient() {
					private MediaScannerConnection msc = null;
					{
						msc = new MediaScannerConnection(
								getApplicationContext(), this);
						msc.connect();
					}

					public void onMediaScannerConnected() {
						msc.scanFile(namePhoto, null);
					}

					public void onScanCompleted(String path, Uri uri) {
						msc.disconnect();
					}
				};
			}
			/**
			 * Recibimos el URI de la imagen y construimos un Bitmap a partir de
			 * un stream de Bytes
			 */
		} else if (requestCode == ChangePhotoHelper.SELECT_PICTURE) {
			Uri selectedImage = data.getData();
			InputStream is;
			try {
				is = getContentResolver().openInputStream(selectedImage);
				BufferedInputStream bis = new BufferedInputStream(is);
				Bitmap bitmap = BitmapFactory.decodeStream(bis);
				ImageView iv = (ImageView) findViewById(R.id.photo);
				iv.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
			}
		}
	}

	@Override
	public void onClick(View v) {
		// Menu menu = null;
		switch (v.getId()) {
		case R.id.photo:
			openContextMenu(v);
			break;

		default:
			break;
		}
	}

	private DatosUsuario getDatosUsuario() {
		DatosUsuario aux = new DatosUsuario();
		aux.setId(_id);
		aux.setCardName(txt_cardName.getText().toString().toUpperCase());
		aux.setName(txt_name.getText().toString());
		aux.setSurname(txt_surname.getText().toString());
		aux.setHomeCell(txt_home_cell.getText().toString());
		aux.setNotes(txt_notes.getText().toString().trim());

		return aux;
	}

	private void setDatosUsuarioToActivity(DatosUsuario datos) {
		txt_cardName.setText(datos.getCardName());
		txt_name.setText(datos.getName());
		txt_surname.setText(datos.getSurname());
		txt_home_cell.setText(datos.getHomeCell());
		txt_notes.setText(datos.getNotes());
		_id = datos.getId();
	}

	/** mEtodo de generaciOn de la nueva tarjeta */
	private void createCard() {
		/**
		 * agregando los datos ingresados a la clase de DatosUsuario que se
		 * devolverA
		 */
		new_card = getDatosUsuario();

		Intent intnt_resp = new Intent();
		intnt_resp.putExtra(DatosUsuario.CLASS_NAME, new_card);

		/**
		 * se devuelve al anterior activity el objeto data y RESULT_OK para
		 * poder saber que no hubo errores
		 */
		setResult(MainActivity.UPDATE_CARD, intnt_resp);
		/** mensaje Toast */
		Toast.makeText(getApplicationContext(),
				new_card.getCardName() + " " + str_state, Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public void onBackPressed() {
		Log.d("CreateActivity", "onBackPressed -> cancelCreate");
		cancelCreate();
	}

	@Override
	protected void onDestroy() {
		Log.i("CreateCard", "Fin de la actividad");
		super.onDestroy();
	}

}
