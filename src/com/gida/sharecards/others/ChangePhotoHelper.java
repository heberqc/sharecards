package com.gida.sharecards.others;

import java.io.File;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;

import com.gida.sharecards.vcard.VCard;

@SuppressLint("SimpleDateFormat")
public class ChangePhotoHelper {

	public static final int TAKE_PICTURE = 1;
	public static final int TAKE_PREVIEW = 2;
	public static final int SELECT_PICTURE = 3;

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	static Date date = new Date();
	static String name = sdf.format(date);

	private static File dir = VCard.getDir(null);
	ImageView iv;

	Context context;

	public ChangePhotoHelper() {
	}

	public static String getNamePhoto() {
		return dir.getAbsolutePath() + "/" + name + ".jpg";
	}
}
