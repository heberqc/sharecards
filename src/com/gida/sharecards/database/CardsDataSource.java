package com.gida.sharecards.database;

import java.util.ArrayList;
import java.util.List;

import com.gida.sharecards.bean.DatosUsuario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CardsDataSource {

	private SQLiteDatabase database;
	private MySQLiteOpenHelper dbHelper;
	private String[] allColumns = { MySQLiteOpenHelper.COLUMN_ID,
			MySQLiteOpenHelper.COLUMN_CARD_NAME,
			MySQLiteOpenHelper.COLUMN_NAME, MySQLiteOpenHelper.COLUMN_SURNAME,
			MySQLiteOpenHelper.COLUMN_HOME_CELL,
			MySQLiteOpenHelper.COLUMN_NOTES };

	public CardsDataSource(Context context) {
		dbHelper = new MySQLiteOpenHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	// crear nuevo registro en la base de datos
		public void addCard(DatosUsuario datos) {
			ContentValues values = new ContentValues();
			values.put(MySQLiteOpenHelper.COLUMN_CARD_NAME, datos.getCardName());
			values.put(MySQLiteOpenHelper.COLUMN_NAME, datos.getName());
			values.put(MySQLiteOpenHelper.COLUMN_SURNAME, datos.getSurname());
			values.put(MySQLiteOpenHelper.COLUMN_HOME_CELL, datos.getHomeCell());
			values.put(MySQLiteOpenHelper.COLUMN_NOTES, datos.getNotes());

			long insertId = database.insert(MySQLiteOpenHelper.TABLE_CARDS_INFO,
					null, values);
			Cursor cursor = database.query(MySQLiteOpenHelper.TABLE_CARDS_INFO,
					allColumns, MySQLiteOpenHelper.COLUMN_ID + " = " + insertId,
					null, null, null, null);
			cursor.moveToFirst();
			cursor.close();
		}
	
	// obtenciOn de un comment identificado por id
	public DatosUsuario getComment_id(long id) {

		Cursor cursor = database.query(MySQLiteOpenHelper.TABLE_CARDS_INFO,
				allColumns, MySQLiteOpenHelper.COLUMN_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		return cursorToComment(cursor);
	}

	// mEtodo de actualizaciOn de una tarjeta
	public int updateCard(DatosUsuario card) {

		ContentValues values = new ContentValues();
		values.put(MySQLiteOpenHelper.COLUMN_CARD_NAME, card.getCardName());
		values.put(MySQLiteOpenHelper.COLUMN_NAME, card.getName());
		values.put(MySQLiteOpenHelper.COLUMN_SURNAME, card.getSurname());
		values.put(MySQLiteOpenHelper.COLUMN_HOME_CELL, card.getHomeCell());
		values.put(MySQLiteOpenHelper.COLUMN_NOTES, card.getNotes());

		return database.update(MySQLiteOpenHelper.TABLE_CARDS_INFO, values,
				MySQLiteOpenHelper.COLUMN_ID + "=?",
				new String[] { String.valueOf(card.getId()) });
	}
	
	// eliminar registro de la base de datos ingresando un DatosUsuario
		public void deleteCard(DatosUsuario datos) {
			long id = datos.getId();
			database.delete(MySQLiteOpenHelper.TABLE_CARDS_INFO,
					MySQLiteOpenHelper.COLUMN_ID + " = " + id, null);
			database.close();
		}

	// obtenciOn de un List<DatosUsuario> con los datos de toda la tabla de
	// tarjetas de la base de datos
	public List<DatosUsuario> getAllComments() {
		List<DatosUsuario> comments = new ArrayList<DatosUsuario>();

		Cursor cursor = database.query(MySQLiteOpenHelper.TABLE_CARDS_INFO,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			DatosUsuario comment = cursorToComment(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}

		cursor.close();
		return comments;
	}

	// conversiOn de un Cursor de la base de datos a un objeto DatosUsuario
	private DatosUsuario cursorToComment(Cursor cursor) {
		DatosUsuario card = new DatosUsuario();
		card.setId(cursor.getLong(0));
		card.setCardName(cursor.getString(1));
		card.setName(cursor.getString(2));
		card.setSurname(cursor.getString(3));
		card.setHomeCell(cursor.getString(4));
		card.setNotes(cursor.getString(5));
		return card;
	}

}
